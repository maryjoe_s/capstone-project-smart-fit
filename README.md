# Flask Project = Python-based web app run on the mirror

### Environmental Requirements

 1. Python3
 2. Opencv
 3. Flask
 4. Flask-SQLAlchemy
 5. flask-bcrypt
 6. flask_login
 7. flask-wtf
### Run the Program
    python run.py
### Account Info
 - user name: Jingyu
 - password: 123456

# mirrar_mall = Java-based web-app run on the PC end

### Environmental Requirements

 1. Java
 2. MySQL
 3. MVN

### How to make the app run on your computer
1. Unzip **.../mirrar_mall/src/main/resources/upload.zip,** and put this file in your computer somewhere
2. modify the file **.../mirrar_mall/src/main/java/ltd/mirrar/mall/common/Constants.java** line 7:
```java
//public final static String FILE_UPLOAD_DIC = "/opt/image/upload/";    for Linux
//public final static String FILE_UPLOAD_DIC ="D:\\upload\\";           for windows
public final static String FILE_UPLOAD_DIC = "/Users/zhangjingyu/Documents/upload/";
```
3. run sql on your terminal, and add the file **../mirrar_mall/src/main/*resources/mirrarMall_schema.sql ** to your local sql database at your end.

4. modify the file **../mirrar_mall/src/main/resources/application.properties**

```java
spring.datasource.url=jdbc:mysql://localhost:3306/mirrarMall_db?useUnicode=true&serverTimezone=Asia/Shanghai&characterEncoding=utf8&autoReconnect=true&useSSL=false&allowMultiQueries=true
spring.datasource.username=root //<-write your username
spring.datasource.password=123456  //<-write your password here
```

5. run applicarionthe by using terminal 
```
zhangjingyu@ZHANGs-MacBook-Pro ~ % cd Desktop
zhangjingyu@ZHANGs-MacBook-Pro Desktop % cd mirrar_mall
zhangjingyu@ZHANGs-MacBook-Pro mirrar_mall % mvn spring-boot:run
```

6. go to `localhost:28089` 

### Account Info
 - user name: Jingyu
 - password: 123456


import cv2

# def gen_frames():
#     vc = cv2.VideoCapture(0)
#     while True:
#         success, frame = vc.read()  # read the camera frame
#         if not success:
#             break
#         else:
#             ret, buffer = cv2.imencode('.jpg', frame)
#             frame = buffer.tobytes()
#             yield (b'--frame\r\n'
#                    b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result
#
#
from project import app

if __name__ == '__main__':
    app.run(debug=True)

import cv2
# import imutils
# from imutils.video import WebcamVideoStream
# from imutils.video import VideoStream
# from imutils.video import FPS
# import numpy as np
# import argparse
# import time
from project.static.algorithm.ARCode.tshirtAugmentation import sWebcamAugment




def gen_frames():
    # sWebcamAugment.augment_tshirt()
    data = sWebcamAugment.augment_tshirt()
    frame = data[0]
    yield (b'--frame\r\n'
           b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result
    # yield (b'--frame\r\n'
    #        b'Content-Type: image/jpeg\r\n'
    #        b'Content-Length: ' + data[1] + b'\r\n'
    #                                       b'\r\n' + frame + b'\r\n')
    # camera = cv2.VideoCapture(0)
    # while True:
    #     frame = camera.read()  # read the camera frame
    #
    #
    #     ret, buffer = cv2.imencode('.jpg', frame)
    #     frame = buffer.tobytes()
    #     yield (b'--frame\r\n'
    #          b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result

    # vs = VideoStream(src=0).start()
    # time.sleep(2.0)
    # while True:
    #     frame = vs.read()
    #     frame = imutils.resize(frame, width=500)
    #     rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    #     cv2.imwrite('t.jpg', frame)
    #     yield (b'--frame\r\n'
    #            b'Content-Type: image/jpeg\r\n\r\n' + open('t.jpg', 'rb').read() + b'\r\n')

from flask import render_template, url_for, flash, redirect, Response
from project import app, db, bcrypt
from project.forms import LogInForm, RegistrationForm
from project.models import User, Item
from flask_login import login_user, current_user, logout_user
# from project.getFrames import gen_frames
from project.getFrames import gen_frames
import cv2
from project.static.algorithm.deep_learning_object_detection_realtime_video import DeepLearning
from project.static.algorithm.ARCode.tshirtAugmentation.sWebcamAugment import WebcamAugment


# from project.sWebcamAugment import augment_tshirt

posts = [
    {
        'user': "Jingyu",
        'password': '123456',
        'item': 'T-shirt'
    },
    {
        'user': "MJ",
        'password': '123456',
        'item': 'Trouser'
    }
]

items = [
    {
        'itemID': "100001",
        'itemImage': '',
        'itemType': 'T-shirt'
    },
    {
        'itemID': "100002",
        'itemImage': '',
        'itemType': 'T-shirt'
    }
]


@app.route('/')
@app.route('/home')
def home():  # put application's code here
    return render_template('home.html', title='Home', posts=posts)


# @app.route('/about')
# def about():  # put application's code here
#     return 'About Page'


@app.route('/register')
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash().decode('utf-8')
        user = User(username=form.username.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash(f'Account created for {form.username.data}!', 'success')
        redirect(url_for(home))
    return render_template('register.html', title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LogInForm()
    if form.validate_on_submit():
        # if form.username.data == 'jingyu' and form.password.data == '123456':
        #     flash('You have been logged in!', 'success')
        #     return redirect(url_for('home'))
        # else:
        #     flash('Login Unsuccessful. Please check username and password', 'danger')
        user = User.query.filter_by(username=form.username.data).first()
        if user and (form.password.data == user.password):
            login_user(user, False)
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
    return render_template('login.html', title='Login', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route('/tryon')
def tryon():
    return render_template('tryon.html')


@app.route('/camera', methods=['POST'])
def camera():
    cap = cv2.VideoCapture(0)
    while True:
        ret, img = cap.read()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite("static/cam.png", img)

        # return render_template("camera.html",result=)
        time.sleep(0.1)
        return json.dumps({'status': 'OK', 'result': "static/cam.png"})
        if cv2.waitKey(0) & 0xFF == ord('q'):
            break
    cap.release()
    return json.dumps({'status': 'OK', 'result': "static/cam.png"})


def gen(camera):
    while True:
        data = camera.augment_tshirt()
        frame = data[0]
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


# @app.route('/video_feed')
# def video_feed():
#     return Response(gen(WebcamAugment()), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/video_feed')
def video_feed():
    call_obj_detc = DeepLearning().deepLearning()
    if call_obj_detc:
        return Response(gen(WebcamAugment()), mimetype='multipart/x-mixed-replace; boundary=frame')
    else:
        flash('Cannot detect a person', 'warning')
    return redirect(url_for('home'))




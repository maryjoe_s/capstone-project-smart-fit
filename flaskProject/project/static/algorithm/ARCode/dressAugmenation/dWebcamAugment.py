import cv2 as cv ##pip install opencv-python
import matplotlib.pyplot as plt
import argparse
from math import hypot
import time
from PIL import Image

net = cv.dnn.readNetFromTensorflow("graph_opt.pb") #weights
inWidth = 368
inHeight = 368
thr = 0.2

BODY_PARTS = { "Nose": 0, "Neck": 1, "RShoulder": 2, "RElbow": 3, "RWrist": 4,
                "LShoulder": 5, "LElbow": 6, "LWrist": 7, "RHip": 8, "RKnee": 9,
                "RAnkle": 10, "LHip": 11, "LKnee": 12, "LAnkle": 13, "REye": 14,
                "LEye": 15, "REar": 16, "LEar": 17, "Background": 18 }

POSE_PAIRS = [ ["Neck", "RShoulder"], ["Neck", "LShoulder"], ["RShoulder", "RElbow"],
                ["RElbow", "RWrist"], ["LShoulder", "LElbow"], ["LElbow", "LWrist"],
                ["Neck", "RHip"], ["RHip", "RKnee"], ["RKnee", "RAnkle"], ["Neck", "LHip"],
                ["LHip", "LKnee"], ["LKnee", "LAnkle"], ["Neck", "Nose"], ["Nose", "REye"],
                ["REye", "REar"], ["Nose", "LEye"], ["LEye", "LEar"] ]


def augment_dresses():
    
    ##perform on webcam
    cap = cv.VideoCapture(0)

    cap.set(cv.CAP_PROP_FPS, 10)
    cap.set(3,800)
    cap.set(4,800)

    if not cap.isOpened():
        cap = cv.VideoCapture(0)
    if not cap.isOpened():
        raise IOError("Can't open webcam")

    while cv.waitKey(1) < 0:
        hasFrame, frame = cap.read()
        if not hasFrame:
            cv.waitKey()
            break
        
        dresses_img = cv.imread("dresses.png")

        #get the dresses img dimensions(w x h)
        im = Image.open("dresses.png")
        d_width, d_height = im.size

        frameWidth = frame.shape[1]
        frameHeight = frame.shape[0]
        net.setInput(cv.dnn.blobFromImage(frame, 1.0, (inWidth, inHeight), (127.5, 127.5, 127.5), swapRB=True, crop=False))
        out = net.forward()
        out = out[:, :19, :, :]  # MobileNet output [1, 57, -1, -1], we only need the first 19 elements

        assert(len(BODY_PARTS) == out.shape[1])

        points = [] ##list for get points of the body parts

        for i in range(len(BODY_PARTS)):
            # Slice heatmap of corresponging body's part.
            heatMap = out[0, i, :, :]

            # Originally, we try to find all the local maximums. To simplify a sample
            # we just find a global one. However only a single pose at the same time
            # could be detected this way.
            _, conf, _, point = cv.minMaxLoc(heatMap)
            x = (frameWidth * point[0]) / out.shape[3]
            y = (frameHeight * point[1]) / out.shape[2]
            # Add a point if it's confidence is higher than threshold.
            points.append((int(x), int(y)) if conf > thr else None)
        
#         print(points)

        #collect points for dress
        left_shoulder = points[5]
        right_shoulder = points[2]
        neck = points[1]
        left_hip = points[11]
        right_hip = points[8]
        left_knee = points[9]
        right_knee = points[12]
        
        found = (left_knee != None) and (right_knee != None) and (left_hip != None) and (right_hip != None) and (left_shoulder != None) and (right_shoulder != None) and (neck != None)
        
        if found == True:
            #calculate midpoint to augment shirt
            x_midpoint = int ((left_shoulder[0]+right_shoulder[0]+right_knee[0]+left_knee[0])/4)
            y_midpoint = int ((left_shoulder[1]+right_shoulder[1]+right_knee[1]+left_knee[1])/4)
            midpoint = (x_midpoint, y_midpoint)

            print(midpoint)

            # 1.55 = dresses_img_dim*(0.7396924829)
            dresses_width = int (hypot(right_shoulder[0] - left_shoulder[0],
                                right_shoulder[1] - left_shoulder[1]))

            # print(dresses_width)

            #recalculated height based on the dress image dimensions and width
            dresses_height = int(dresses_width * (d_height/d_width))
            # print(dresses_height)

            #rectangle points
            top_left = (int (midpoint[0] - dresses_width / 2),
                        int (midpoint[1] - dresses_height / 2))
            botrom_right = (int(midpoint[0] + dresses_width / 2),
                            int(midpoint[1] + dresses_height / 2))

        #     adding the new dress
            new_dress = cv.resize(dresses_img,( dresses_width,  dresses_height))
            new_dress_gray = cv.cvtColor(new_dress, cv.COLOR_BGR2GRAY)
            _, dress_mask = cv.threshold(new_dress_gray, 25, 255, cv.THRESH_BINARY_INV)

            dress_area = frame[top_left[1]: top_left[1]+  dresses_height,
                              top_left[0]: top_left[0] +  dresses_width]

            dress_area_no_dress = cv.bitwise_and(dress_area, dress_area, mask=dress_mask)
            final_dress = cv.add(dress_area_no_dress, new_dress)

            frame[top_left[1]: top_left[1]+  dresses_height,
                              top_left[0]: top_left[0] + dresses_width] = final_dress

            plt.imshow(dress_area)
            plt.imshow(dresses_img)
            plt.imshow(cv.cvtColor(frame, cv.COLOR_BGR2RGB))

        #     cv.imshow("Dress area", dress_area)
        #     cv.imshow("Show new dress", new_dress)
        #     cv.imshow("Final dress", final_dress)
        
        
        cv.imshow('Dress augmentation', frame)
       
#main
#augment_dresses() 


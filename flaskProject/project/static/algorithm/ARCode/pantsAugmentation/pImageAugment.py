import cv2 as cv ##pip install opencv-python
import matplotlib.pyplot as plt
import argparse
from math import hypot
import time

net = cv.dnn.readNetFromTensorflow("graph_opt.pb") #weights
inWidth = 368
inHeight = 368
thr = 0.2

BODY_PARTS = { "Nose": 0, "Neck": 1, "RShoulder": 2, "RElbow": 3, "RWrist": 4,
               "LShoulder": 5, "LElbow": 6, "LWrist": 7, "RHip": 8, "RKnee": 9,
               "RAnkle": 10, "LHip": 11, "LKnee": 12, "LAnkle": 13, "REye": 14,
               "LEye": 15, "REar": 16, "LEar": 17, "Background": 18 }

POSE_PAIRS = [ ["Neck", "RShoulder"], ["Neck", "LShoulder"], ["RShoulder", "RElbow"],
               ["RElbow", "RWrist"], ["LShoulder", "LElbow"], ["LElbow", "LWrist"],
               ["Neck", "RHip"], ["RHip", "RKnee"], ["RKnee", "RAnkle"], ["Neck", "LHip"],
               ["LHip", "LKnee"], ["LKnee", "LAnkle"], ["Neck", "Nose"], ["Nose", "REye"],
               ["REye", "REar"], ["Nose", "LEye"], ["LEye", "LEar"] ]

def augment_pants(frame):

    pants_img = cv.imread("pants2.png")
    frameWidth = frame.shape[1]
    frameHeight = frame.shape[0]
    net.setInput(cv.dnn.blobFromImage(frame, 1.0, (inWidth, inHeight), (127.5, 127.5, 127.5), swapRB=True, crop=False))
    out = net.forward()
    out = out[:, :19, :, :]  # MobileNet output [1, 57, -1, -1], we only need the first 19 elements

    assert(len(BODY_PARTS) == out.shape[1])

    points = [] ##list for get points of the body parts

    for i in range(len(BODY_PARTS)):
        # Slice heatmap of corresponging body's part.
        heatMap = out[0, i, :, :]

        # Originally, we try to find all the local maximums. To simplify a sample
        # we just find a global one. However only a single pose at the same time
        # could be detected this way.
        _, conf, _, point = cv.minMaxLoc(heatMap)
        x = (frameWidth * point[0]) / out.shape[3]
        y = (frameHeight * point[1]) / out.shape[2]
        # Add a point if it's confidence is higher than threshold.
        points.append((int(x), int(y)) if conf > thr else None)
        
    print(points);

    #get all used points 
    left_knee = points[12]
    right_knee = points[9]
    left_hip = points[11]
    right_hip = points[8]
    left_wrist = points[7]
    right_wrist = points[4]
    left_ankle = points[13]
    right_ankle = points[10]
    
 #calculate midpoint to augment shirt
    x_midpoint = int ((right_wrist[0] + left_wrist[0] + right_ankle[0] + left_ankle[0])/4)
    y_midpoint = int ((right_wrist[1] + left_wrist[1] + right_ankle[1] + left_ankle[1])/4)
    midpoint = (x_midpoint, y_midpoint)

#             print(midpoint)

    pants_width = int (hypot(right_hip[0] - left_hip[0],
                            right_hip[1] - left_hip[1]) * 1.88)

#             print(pants_width)


    #recalculated height based on the pants image dimensions and pants width
    pants_height = int(pants_width * 2.7)
#             print(pants_height)

    #rectangle points
    top_left = (int (midpoint[0] - pants_width / 2),
                int (midpoint[1] - pants_height / 2))
    bottom_right = (int(midpoint[0] + pants_width / 2),
                    int(midpoint[1] + pants_height / 2))

#     adding the new pants
    new_pants = cv.resize(pants_img,(pants_width, pants_height))
    new_pants_gray = cv.cvtColor(new_pants, cv.COLOR_BGR2GRAY)
    _, pants_mask = cv.threshold(new_pants_gray, 25, 255, cv.THRESH_BINARY_INV)

    pants_area = frame[top_left[1]: top_left[1]+ pants_height,
                      top_left[0]: top_left[0] + pants_width]

    pants_area_no_pants = cv.bitwise_and(pants_area, pants_area, mask=pants_mask)
    final_pants = cv.add(pants_area_no_pants, new_pants)

    frame[top_left[1]: top_left[1]+ pants_height,
                      top_left[0]: top_left[0] + pants_width] = final_pants

    plt.imshow(pants_area)
    plt.imshow(pants_img)
    plt.imshow(cv.cvtColor(frame, cv.COLOR_BGR2RGB))

#     cv.imshow("Pants area", pants_area)
#     cv.imshow("Show new pants", new_pants)
#     cv.imshow("Final pants", final_pants)
    cv.imshow("Original", frame)
    cv.waitKey(0)


augment_pants(img);

    

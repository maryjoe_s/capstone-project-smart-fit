import cv2 as cv ##pip install opencv-python
import matplotlib.pyplot as plt
import argparse
from math import hypot
import time

net = cv.dnn.readNetFromTensorflow("graph_opt.pb") #weights
inWidth = 368
inHeight = 368
thr = 0.2

BODY_PARTS = { "Nose": 0, "Neck": 1, "RShoulder": 2, "RElbow": 3, "RWrist": 4,
               "LShoulder": 5, "LElbow": 6, "LWrist": 7, "RHip": 8, "RKnee": 9,
               "RAnkle": 10, "LHip": 11, "LKnee": 12, "LAnkle": 13, "REye": 14,
               "LEye": 15, "REar": 16, "LEar": 17, "Background": 18 }

POSE_PAIRS = [ ["Neck", "RShoulder"], ["Neck", "LShoulder"], ["RShoulder", "RElbow"],
               ["RElbow", "RWrist"], ["LShoulder", "LElbow"], ["LElbow", "LWrist"],
               ["Neck", "RHip"], ["RHip", "RKnee"], ["RKnee", "RAnkle"], ["Neck", "LHip"],
               ["LHip", "LKnee"], ["LKnee", "LAnkle"], ["Neck", "Nose"], ["Nose", "REye"],
               ["REye", "REar"], ["Nose", "LEye"], ["LEye", "LEar"] ]

img = cv.imread("image.png")

def augment_tshirt(frame):

    shirt_img = cv.imread("tshirt.png")
    frameWidth = frame.shape[1]
    frameHeight = frame.shape[0]
    net.setInput(cv.dnn.blobFromImage(frame, 1.0, (inWidth, inHeight), (127.5, 127.5, 127.5), swapRB=True, crop=False))
    out = net.forward()
    out = out[:, :19, :, :]  # MobileNet output [1, 57, -1, -1], we only need the first 19 elements

    assert(len(BODY_PARTS) == out.shape[1])

    points = [] ##list for get points of the body parts

    for i in range(len(BODY_PARTS)):
        # Slice heatmap of corresponging body's part.
        heatMap = out[0, i, :, :]

        # Originally, we try to find all the local maximums. To simplify a sample
        # we just find a global one. However only a single pose at the same time
        # could be detected this way.
        _, conf, _, point = cv.minMaxLoc(heatMap)
        x = (frameWidth * point[0]) / out.shape[3]
        y = (frameHeight * point[1]) / out.shape[2]
        # Add a point if it's confidence is higher than threshold.
        points.append((int(x), int(y)) if conf > thr else None)
        
    print(points);

    #collect points for tshirt
    left_shoulder = points[5]
    right_shoulder = points[2]
    neck = points[1]
    left_hip = points[11]
    right_hip = points[8]
    left_elbow = points[6]
    right_elbow = points[3]
    
    #calculate midpoint to augment shirt
    x_midpoint = int ((left_shoulder[0]+right_shoulder[0]+right_hip[0]+left_hip[0])/4)
    y_midpoint = int ((left_shoulder[1]+right_shoulder[1]+right_hip[1]+left_hip[1])/4)
    midpoint = (x_midpoint, y_midpoint)

    print(midpoint)

    shirt_width = int (hypot(right_elbow[0] - left_elbow[0],
                       right_elbow[1] - left_elbow[1]) *1.23)

    print(shirt_width)

    shirt_height = hypot(neck[0] - right_hip[0],
                       neck[1] - right_hip[1])

    #recalculated height based on the shirt image dimensions
    shirt_height = int(shirt_width * 1.15)
    print(shirt_height)

    #rectangle points
    top_left = (int (midpoint[0] - shirt_width / 2),
                int (midpoint[1] - shirt_height / 2))
    botrom_right = (int(midpoint[0] + shirt_width / 2),
                    int(midpoint[1] + shirt_height / 2))
    
#     adding the new shirt
    new_shirt = cv.resize(shirt_img,(shirt_width, shirt_height))
    new_shirt_gray = cv.cvtColor(new_shirt, cv.COLOR_BGR2GRAY)
    _, shirt_mask = cv.threshold(new_shirt_gray, 25, 255, cv.THRESH_BINARY_INV)
    
    shirt_area = frame[top_left[1]: top_left[1]+ shirt_height,
                      top_left[0]: top_left[0] + shirt_width]
    
    shirt_area_no_shirt = cv.bitwise_and(shirt_area, shirt_area, mask=shirt_mask)
    final_shirt = cv.add(shirt_area_no_shirt, new_shirt)
    
    frame[top_left[1]: top_left[1]+ shirt_height,
                      top_left[0]: top_left[0] + shirt_width] = final_shirt

    plt.imshow(shirt_area)
    plt.imshow(shirt_img)
    plt.imshow(cv.cvtColor(frame, cv.COLOR_BGR2RGB))
    
#     cv.imshow("Shirt area", shirt_area)
#     cv.imshow("Show new shirt", new_shirt)
#     cv.imshow("Final shirt", final_shirt)
    cv.imshow("Original", frame)
    cv.waitKey(0)

augment_tshirt(img)
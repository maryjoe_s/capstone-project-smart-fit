import cv2 as cv  ##pip install opencv-python
import matplotlib.pyplot as plt
import argparse
from math import hypot
import time
from PIL import Image

net = cv.dnn.readNetFromTensorflow("graph_opt.pb")  # weights
inWidth = 368
inHeight = 368
thr = 0.2

BODY_PARTS = {"Nose": 0, "Neck": 1, "RShoulder": 2, "RElbow": 3, "RWrist": 4,
              "LShoulder": 5, "LElbow": 6, "LWrist": 7, "RHip": 8, "RKnee": 9,
              "RAnkle": 10, "LHip": 11, "LKnee": 12, "LAnkle": 13, "REye": 14,
              "LEye": 15, "REar": 16, "LEar": 17, "Background": 18}

POSE_PAIRS = [["Neck", "RShoulder"], ["Neck", "LShoulder"], ["RShoulder", "RElbow"],
              ["RElbow", "RWrist"], ["LShoulder", "LElbow"], ["LElbow", "LWrist"],
              ["Neck", "RHip"], ["RHip", "RKnee"], ["RKnee", "RAnkle"], ["Neck", "LHip"],
              ["LHip", "LKnee"], ["LKnee", "LAnkle"], ["Neck", "Nose"], ["Nose", "REye"],
              ["REye", "REar"], ["Nose", "LEye"], ["LEye", "LEar"]]


class WebcamAugment(object):

    def __init__(self):
        self.cap = cv.VideoCapture(0)

    def __del__(self):
        self.stream.stop()

    def augment_tshirt(self):
        ##perform on webcam
        # cap = cv.VideoCapture(0)
        self.cap.set(cv.CAP_PROP_FPS, 10)
        # self.cap.set(3, 800)
        # self.cap.set(4, 800)

        if not self.cap.isOpened():
            self.cap = cv.VideoCapture(0)
        if not self.cap.isOpened():
            raise IOError("Can't open webcam")

        while cv.waitKey(1) < 0:
            hasFrame, frame = self.cap.read()
            if not hasFrame:
                cv.waitKey()
                break

            shirt_img = cv.imread("tshirt.png")

            # get the tshirt img dimensions(w x h)
            im = Image.open('tshirt.png')
            s_width, s_height = im.size

            # print(s_width)
            # print(s_height)

            frameWidth = frame.shape[1]
            frameHeight = frame.shape[0]
            net.setInput(
                cv.dnn.blobFromImage(frame, 1.0, (inWidth, inHeight), (127.5, 127.5, 127.5), swapRB=True, crop=False))
            out = net.forward()
            out = out[:, :19, :, :]  # MobileNet output [1, 57, -1, -1], we only need the first 19 elements

            assert (len(BODY_PARTS) == out.shape[1])

            points = []  ##list for get points of the body parts

            for i in range(len(BODY_PARTS)):
                # Slice heatmap of corresponging body's part.
                heatMap = out[0, i, :, :]

                # Originally, we try to find all the local maximums. To simplify a sample
                # we just find a global one. However only a single pose at the same time
                # could be detected this way.
                _, conf, _, point = cv.minMaxLoc(heatMap)
                x = (frameWidth * point[0]) / out.shape[3]
                y = (frameHeight * point[1]) / out.shape[2]
                # Add a point if it's confidence is higher than threshold.
                points.append((int(x), int(y)) if conf > thr else None)

            print(points)

            # get all used points
            left_shoulder = points[5]
            right_shoulder = points[2]
            neck = points[1]
            left_hip = points[11]
            right_hip = points[8]
            left_elbow = points[6]
            right_elbow = points[3]

            found = (left_shoulder != None) and (right_shoulder != None) and (neck != None) and (left_hip != None) and (
                    right_hip != None) and (left_elbow != None) and (right_elbow != None)

            if found:
                # print(right_elbow)

                # calculate midpoint to augment shirt
                x_midpoint = int((left_shoulder[0] + right_shoulder[0] + right_hip[0] + left_hip[0]) / 4)
                y_midpoint = int((left_shoulder[1] + right_shoulder[1] + right_hip[1] + left_hip[1]) / 4)
                midpoint = (x_midpoint, y_midpoint)

                print(midpoint)

                # 1.30 = tshirt_img_dim* 1.2676)
                shirt_width = int(hypot(right_elbow[0] - left_elbow[0], right_elbow[1] - left_elbow[1]))
                # print(shirt_width)

                # calculate height based on the shirt image dimensions and the calculated width
                shirt_height = int(shirt_width * (s_height / s_width))
                # print(shirt_height)

                # rectangle points
                top_left = (int(midpoint[0] - shirt_width / 2),
                            int(midpoint[1] - shirt_height / 2))
                bottom_right = (int(midpoint[0] + shirt_width / 2),
                                int(midpoint[1] + shirt_height / 2))

                # adding the new shirt
                new_shirt = cv.resize(shirt_img, (shirt_width, shirt_height))

                new_shirt_gray = cv.cvtColor(new_shirt, cv.COLOR_BGR2GRAY)
                _, shirt_mask = cv.threshold(new_shirt_gray, 25, 255, cv.THRESH_BINARY_INV)

                shirt_area = frame[top_left[1]: top_left[1] + shirt_height,
                             top_left[0]: top_left[0] + shirt_width]

                shirt_area_no_shirt = cv.bitwise_and(shirt_area, shirt_area, mask=shirt_mask)
                final_shirt = cv.add(shirt_area_no_shirt, new_shirt)

                frame[top_left[1]: top_left[1] + shirt_height,
                top_left[0]: top_left[0] + shirt_width] = final_shirt


            ret, jepg = cv.imencode('.jpg', frame)
            data = []
            data.append(jepg.tobytes())
            return data

            #             plt.imshow(shirt_area)
            #             plt.imshow(shirt_img)
            #             plt.imshow(cv.cvtColor(frame, cv.COLOR_BGR2RGB))

            #             cv.imshow("Shirt area", shirt_area)
            #             cv.imshow("Show new shirt", new_shirt)
            #         cv.imshow("Final shirt", final_shirt)
            # cv.imshow('Shirt augmentation', frame)

            # frame = buffer.tobytes()
            # yield (b'--frame\r\n'
            #        b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # concat frame one by one and show result
            # yield (b'--frame\r\n'
            #        b'Content-Type: image/jpeg\r\n'
            #        b'Content-Length: ' + frameHeight + b'\r\n'
            #                                       b'\r\n' + frame + b'\r\n')
            # time.sleep(0.01)  # my Firefox needs some time to display image / Chrome displays image without it

    # main
    # augment_tshirt()

For Deep Learning, 
MobileNet-SSD was trained to detect humans among other objects such as a chair, a table, a bottle, etc. 
MobileNet was selected for our project since it is a convolutional neural network that is used in cases of image processing and object detection
Our own has 47 convolution layers.

For Object Detection,
To achieve object detection, pose estimation was used.
To implement pose estimation for the AR software the COCO keypoints challenge was utilized.

To run,
```bash 
python deep_learning_object_detection_realtime_video.py
```

For Augmentation, 
In terms of augmentation, development for overlaying clothes on an image has been established. 
This was achievable by 
- Performing vector calculations to find the needed parameters 
- Using methods from the OpenCV package for the overlaying part

Last Updated: September 23, 2021 

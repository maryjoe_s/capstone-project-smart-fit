package ltd.mirrar.mall;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("ltd.mirrar.mall.dao")
@SpringBootApplication
public class MirrarMallApplication {

    public static void main(String[] args) {
        SpringApplication.run(MirrarMallApplication.class, args);
    }

}

package ltd.mirrar.mall.common;


public class Constants {
    //public final static String FILE_UPLOAD_DIC = "/opt/image/upload/";    for Linux
    //public final static String FILE_UPLOAD_DIC ="D:\\upload\\";           for windows
    public final static String FILE_UPLOAD_DIC = "/Users/jingyu/Documents/upload/";

    public final static int INDEX_CAROUSEL_NUMBER = 5;

    public final static int INDEX_CATEGORY_NUMBER = 10;

    public final static int SEARCH_CATEGORY_NUMBER = 8;

    public final static int INDEX_GOODS_HOT_NUMBER = 4;

    public final static int SHOPPING_CART_ITEM_TOTAL_NUMBER = 13;

    public final static int SHOPPING_CART_ITEM_LIMIT_NUMBER = 5;

    public final static String MALL_VERIFY_CODE_KEY = "mallVerifyCode";

    public final static String MALL_USER_SESSION_KEY = "MallUser";

    public final static int GOODS_SEARCH_PAGE_LIMIT = 10;

    public final static int ORDER_SEARCH_PAGE_LIMIT = 3;

    public final static int SELL_STATUS_UP = 0;
    public final static int SELL_STATUS_DOWN = 1;

}

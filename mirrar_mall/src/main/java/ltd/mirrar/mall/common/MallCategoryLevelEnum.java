package ltd.mirrar.mall.common;

/**
 * @apiNote MallCategoryLevelEnum
 */
public enum MallCategoryLevelEnum {

    DEFAULT(0, "ERROR"),
    LEVEL_ONE(1, "LEVEL_ONE"),
    LEVEL_TWO(2, "LEVEL_TWO"),
    LEVEL_THREE(3, "LEVEL_THREE");

    private int level;

    private String name;

    MallCategoryLevelEnum(int level, String name) {
        this.level = level;
        this.name = name;
    }

    public static MallCategoryLevelEnum getMallOrderStatusEnumByLevel(int level) {
        for (MallCategoryLevelEnum MallCategoryLevelEnum : MallCategoryLevelEnum.values()) {
            if (MallCategoryLevelEnum.getLevel() == level) {
                return MallCategoryLevelEnum;
            }
        }
        return DEFAULT;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

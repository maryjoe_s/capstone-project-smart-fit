package ltd.mirrar.mall.common;

/**
 * @apiNote orderStatus:0.Unpaid 1.Paid 2.Ready 3:Shipped 4.Completed -1."Cancelled" -2.Time-out -3.Closed
 */
public enum MallOrderStatusEnum {

    DEFAULT(-9, "ERROR"),
    ORDER_PRE_PAY(0, "Unpaid"),
    OREDER_PAID(1, "Paid"),
    OREDER_PACKAGED(2, "Ready"),
    OREDER_EXPRESS(3, "Shipped"),
    ORDER_SUCCESS(4, "Completed"),
    ORDER_CLOSED_BY_MALLUSER(-1, "Cancelled"),
    ORDER_CLOSED_BY_EXPIRED(-2, "Time-out"),
    ORDER_CLOSED_BY_JUDGE(-3, "Closed");

    private int orderStatus;

    private String name;

    MallOrderStatusEnum(int orderStatus, String name) {
        this.orderStatus = orderStatus;
        this.name = name;
    }

    public static MallOrderStatusEnum getMallOrderStatusEnumByStatus(int orderStatus) {
        for (MallOrderStatusEnum MallOrderStatusEnum : MallOrderStatusEnum.values()) {
            if (MallOrderStatusEnum.getOrderStatus() == orderStatus) {
                return MallOrderStatusEnum;
            }
        }
        return DEFAULT;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package ltd.mirrar.mall.common;

public enum ServiceResultEnum {
    ERROR("error"),

    SUCCESS("success"),

    DATA_NOT_EXIST("Data not existed！"),

    SAME_CATEGORY_EXIST("Same category existed！"),

    SAME_LOGIN_NAME_EXIST("Same user account exist！"),

    LOGIN_NAME_NULL("Please enter user account！"),

    LOGIN_PASSWORD_NULL("Please enter password！"),

    LOGIN_VERIFY_CODE_NULL("Please enter verify code！"),

    LOGIN_VERIFY_CODE_ERROR("Wrong verify code！"),

    GOODS_NOT_EXIST("Goods not exist！"),

    GOODS_PUT_DOWN("Goods no more available！"),

    SHOPPING_CART_ITEM_LIMIT_NUMBER_ERROR("Exceed limited number for each goods！"),

    SHOPPING_CART_ITEM_TOTAL_NUMBER_ERROR("Exceed limited number for shopping cart！"),

    LOGIN_ERROR("login failure！"),

    LOGIN_USER_LOCKED("Current account have been locked！"),

    ORDER_NOT_EXIST_ERROR("Order not exist！"),

    NULL_ADDRESS_ERROR("Address cannot be empty！"),

    ORDER_PRICE_ERROR("Price Error！"),

    ORDER_GENERATE_ERROR("Order Generator Error！"),

    SHOPPING_ITEM_ERROR("Shopping Cart Error！"),

    SHOPPING_ITEM_COUNT_ERROR("Out of Number！"),

    ORDER_STATUS_ERROR("Oder Status Error！"),

    OPERATE_ERROR("Operate Error！"),

    DB_ERROR("Database Error");

    private String result;

    ServiceResultEnum(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}

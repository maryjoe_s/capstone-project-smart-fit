package ltd.mirrar.mall.controller.mall;

import ltd.mirrar.mall.common.Constants;
import ltd.mirrar.mall.common.IndexConfigTypeEnum;
import ltd.mirrar.mall.controller.vo.MallIndexCarouselVO;
import ltd.mirrar.mall.controller.vo.MallIndexCategoryVO;
import ltd.mirrar.mall.controller.vo.MallIndexConfigGoodsVO;
import ltd.mirrar.mall.service.MallCarouselService;
import ltd.mirrar.mall.service.MallCategoryService;
import ltd.mirrar.mall.service.MallIndexConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class IndexController {

    @Resource
    private MallCarouselService MallCarouselService;

    @Resource
    private MallIndexConfigService MallIndexConfigService;

    @Resource
    private MallCategoryService MallCategoryService;

    @GetMapping({"/index", "/", "/index.html"})
    public String indexPage(HttpServletRequest request) {
        List<MallIndexCategoryVO> categories = MallCategoryService.getCategoriesForIndex();
        if (CollectionUtils.isEmpty(categories)) {
            return "error/error_5xx";
        }
        List<MallIndexCarouselVO> carousels = MallCarouselService.getCarouselsForIndex(Constants.INDEX_CAROUSEL_NUMBER);
        List<MallIndexConfigGoodsVO> hotGoodses = MallIndexConfigService.getConfigGoodsesForIndex(IndexConfigTypeEnum.INDEX_GOODS_HOT.getType(), Constants.INDEX_GOODS_HOT_NUMBER);
        request.setAttribute("categories", categories);
        request.setAttribute("carousels", carousels);
        request.setAttribute("hotGoodses", hotGoodses);

        return "mall/index";
    }
}

package ltd.mirrar.mall.service;

import ltd.mirrar.mall.controller.vo.MallIndexCarouselVO;
import ltd.mirrar.mall.entity.Carousel;
import ltd.mirrar.mall.util.PageQueryUtil;
import ltd.mirrar.mall.util.PageResult;

import java.util.List;

public interface MallCarouselService {
    /**
     * getCarouselPage
     *
     * @param pageUtil
     * @return
     */
    PageResult getCarouselPage(PageQueryUtil pageUtil);

    String saveCarousel(Carousel carousel);

    String updateCarousel(Carousel carousel);

    Carousel getCarouselById(Integer id);

    Boolean deleteBatch(Integer[] ids);

    /**
     * getCarouselsForIndex
     *
     * @param number
     * @return
     */
    List<MallIndexCarouselVO> getCarouselsForIndex(int number);
}

package ltd.mirrar.mall.service;

import ltd.mirrar.mall.controller.vo.MallIndexCategoryVO;
import ltd.mirrar.mall.controller.vo.SearchPageCategoryVO;
import ltd.mirrar.mall.entity.GoodsCategory;
import ltd.mirrar.mall.util.PageQueryUtil;
import ltd.mirrar.mall.util.PageResult;

import java.util.List;

public interface MallCategoryService {
    /**
     * getCategorisPage
     *
     * @param pageUtil
     * @return
     */
    PageResult getCategorisPage(PageQueryUtil pageUtil);

    String saveCategory(GoodsCategory goodsCategory);

    String updateGoodsCategory(GoodsCategory goodsCategory);

    GoodsCategory getGoodsCategoryById(Long id);

    Boolean deleteBatch(Integer[] ids);

    /**
     * getCategoriesForIndex
     *
     * @return
     */
    List<MallIndexCategoryVO> getCategoriesForIndex();

    /**
     * getCategoriesForSearch
     *
     * @param categoryId
     * @return
     */
    SearchPageCategoryVO getCategoriesForSearch(Long categoryId);

    /**
     * selectByLevelAndParentIdsAndNumber
     *
     * @param parentIds
     * @param categoryLevel
     * @return
     */
    List<GoodsCategory> selectByLevelAndParentIdsAndNumber(List<Long> parentIds, int categoryLevel);
}

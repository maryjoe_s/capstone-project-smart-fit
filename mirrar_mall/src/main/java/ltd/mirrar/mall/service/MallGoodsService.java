package ltd.mirrar.mall.service;

import ltd.mirrar.mall.entity.MallGoods;
import ltd.mirrar.mall.util.PageQueryUtil;
import ltd.mirrar.mall.util.PageResult;

import java.util.List;

public interface MallGoodsService {
    /**
     * getMallGoodsPage
     *
     * @param pageUtil
     * @return
     */
    PageResult getMallGoodsPage(PageQueryUtil pageUtil);

    /**
     * saveMallGoods
     *
     * @param goods
     * @return
     */
    String saveMallGoods(MallGoods goods);

    /**
     * batchSaveMallGoods
     *
     * @param MallGoodsList
     * @return
     */
    void batchSaveMallGoods(List<MallGoods> MallGoodsList);

    /**
     * updateMallGoods
     *
     * @param goods
     * @return
     */
    String updateMallGoods(MallGoods goods);

    /**
     * getMallGoodsById
     *
     * @param id
     * @return
     */
    MallGoods getMallGoodsById(Long id);

    /**
     * batchUpdateSellStatus
     *
     * @param ids
     * @return
     */
    Boolean batchUpdateSellStatus(Long[] ids,int sellStatus);

    /**
     * searchMallGoods
     *
     * @param pageUtil
     * @return
     */
    PageResult searchMallGoods(PageQueryUtil pageUtil);
}

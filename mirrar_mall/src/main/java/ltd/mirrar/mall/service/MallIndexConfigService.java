package ltd.mirrar.mall.service;

import ltd.mirrar.mall.controller.vo.MallIndexConfigGoodsVO;
import ltd.mirrar.mall.entity.IndexConfig;
import ltd.mirrar.mall.util.PageQueryUtil;
import ltd.mirrar.mall.util.PageResult;

import java.util.List;

public interface MallIndexConfigService {
    /**
     * getConfigsPage
     *
     * @param pageUtil
     * @return
     */
    PageResult getConfigsPage(PageQueryUtil pageUtil);

    String saveIndexConfig(IndexConfig indexConfig);

    String updateIndexConfig(IndexConfig indexConfig);

    IndexConfig getIndexConfigById(Long id);

    /**
     * getConfigGoodsesForIndex
     *
     * @param number
     * @return
     */
    List<MallIndexConfigGoodsVO> getConfigGoodsesForIndex(int configType, int number);

    Boolean deleteBatch(Long[] ids);
}

package ltd.mirrar.mall.service;

import ltd.mirrar.mall.controller.vo.MallOrderDetailVO;
import ltd.mirrar.mall.controller.vo.MallOrderItemVO;
import ltd.mirrar.mall.controller.vo.MallShoppingCartItemVO;
import ltd.mirrar.mall.controller.vo.MallUserVO;
import ltd.mirrar.mall.entity.MallOrder;
import ltd.mirrar.mall.util.PageQueryUtil;
import ltd.mirrar.mall.util.PageResult;

import java.util.List;

public interface MallOrderService {

    PageResult getMallOrdersPage(PageQueryUtil pageUtil);

    String updateOrderInfo(MallOrder MallOrder);

    String checkDone(Long[] ids);

    String checkOut(Long[] ids);

    String closeOrder(Long[] ids);

    String saveOrder(MallUserVO user, List<MallShoppingCartItemVO> myShoppingCartItems);


    MallOrderDetailVO getOrderDetailByOrderNo(String orderNo, Long userId);

    MallOrder getMallOrderByOrderNo(String orderNo);

    PageResult getMyOrders(PageQueryUtil pageUtil);

    String cancelOrder(String orderNo, Long userId);

    String finishOrder(String orderNo, Long userId);

    String paySuccess(String orderNo, int payType);

    List<MallOrderItemVO> getOrderItems(Long id);
}

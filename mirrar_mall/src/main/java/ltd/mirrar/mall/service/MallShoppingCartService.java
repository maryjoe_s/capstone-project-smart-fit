package ltd.mirrar.mall.service;

import ltd.mirrar.mall.controller.vo.MallShoppingCartItemVO;
import ltd.mirrar.mall.entity.MallShoppingCartItem;

import java.util.List;

public interface MallShoppingCartService {

    /**
     * saveMallCartItem
     *
     * @param MallShoppingCartItem
     * @return
     */
    String saveMallCartItem(MallShoppingCartItem MallShoppingCartItem);

    /**
     * updateMallCartItem
     *
     * @param MallShoppingCartItem
     * @return
     */
    String updateMallCartItem(MallShoppingCartItem MallShoppingCartItem);

    MallShoppingCartItem getMallCartItemById(Long MallShoppingCartItemId);

    /**
     * deleteById
     *
     * @param MallShoppingCartItemId
     * @return
     */
    Boolean deleteById(Long MallShoppingCartItemId);

    /**
     * getMyShoppingCartItems
     *
     * @param MallUserId
     * @return
     */
    List<MallShoppingCartItemVO> getMyShoppingCartItems(Long MallUserId);
}

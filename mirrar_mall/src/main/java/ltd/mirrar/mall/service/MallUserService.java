package ltd.mirrar.mall.service;

import ltd.mirrar.mall.controller.vo.MallUserVO;
import ltd.mirrar.mall.entity.MallUser;
import ltd.mirrar.mall.util.PageQueryUtil;
import ltd.mirrar.mall.util.PageResult;

import javax.servlet.http.HttpSession;

public interface MallUserService {
    /**
     * getMallUsersPage
     *
     * @param pageUtil
     * @return
     */
    PageResult getMallUsersPage(PageQueryUtil pageUtil);

    /**
     * register
     *
     * @param loginName
     * @param password
     * @return
     */
    String register(String loginName, String password);

    /**
     * login
     *
     * @param loginName
     * @param passwordMD5
     * @param httpSession
     * @return
     */
    String login(String loginName, String passwordMD5, HttpSession httpSession);

    /**
     * updateUserInfo
     *
     * @param mallUser
     * @return
     */
   MallUserVO updateUserInfo(MallUser mallUser, HttpSession httpSession);

    /**
     * lockUsers(0-unlock 1-locked)
     *
     * @param ids
     * @param lockStatus
     * @return
     */
    Boolean lockUsers(Integer[] ids, int lockStatus);
}

package ltd.mirrar.mall.service.impl;

import ltd.mirrar.mall.common.*;
import ltd.mirrar.mall.controller.vo.*;
import ltd.mirrar.mall.dao.MallGoodsMapper;
import ltd.mirrar.mall.dao.MallOrderItemMapper;
import ltd.mirrar.mall.dao.MallOrderMapper;
import ltd.mirrar.mall.dao.MallShoppingCartItemMapper;
import ltd.mirrar.mall.entity.*;
import ltd.mirrar.mall.service.MallOrderService;
import ltd.mirrar.mall.util.BeanUtil;
import ltd.mirrar.mall.util.NumberUtil;
import ltd.mirrar.mall.util.PageQueryUtil;
import ltd.mirrar.mall.util.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
public class MallOrderServiceImpl implements MallOrderService {

    @Autowired
    private MallOrderMapper MallOrderMapper;
    @Autowired
    private MallOrderItemMapper MallOrderItemMapper;
    @Autowired
    private MallShoppingCartItemMapper MallShoppingCartItemMapper;
    @Autowired
    private MallGoodsMapper MallGoodsMapper;

    @Override
    public PageResult getMallOrdersPage(PageQueryUtil pageUtil) {
        List<MallOrder> MallOrders = MallOrderMapper.findMallOrderList(pageUtil);
        int total = MallOrderMapper.getTotalMallOrders(pageUtil);
        PageResult pageResult = new PageResult(MallOrders, total, pageUtil.getLimit(), pageUtil.getPage());
        return pageResult;
    }

    @Override
    @Transactional
    public String updateOrderInfo(MallOrder MallOrder) {
        MallOrder temp = MallOrderMapper.selectByPrimaryKey(MallOrder.getOrderId());
        if (temp != null && temp.getOrderStatus() >= 0 && temp.getOrderStatus() < 3) {
            temp.setTotalPrice(MallOrder.getTotalPrice());
            temp.setUserAddress(MallOrder.getUserAddress());
            temp.setUpdateTime(new Date());
            if (MallOrderMapper.updateByPrimaryKeySelective(temp) > 0) {
                return ServiceResultEnum.SUCCESS.getResult();
            }
            return ServiceResultEnum.DB_ERROR.getResult();
        }
        return ServiceResultEnum.DATA_NOT_EXIST.getResult();
    }

    @Override
    @Transactional
    public String checkDone(Long[] ids) {
        List<MallOrder> orders = MallOrderMapper.selectByPrimaryKeys(Arrays.asList(ids));
        String errorOrderNos = "";
        if (!CollectionUtils.isEmpty(orders)) {
            for (MallOrder MallOrder : orders) {
                if (MallOrder.getIsDeleted() == 1) {
                    errorOrderNos += MallOrder.getOrderNo() + " ";
                    continue;
                }
                if (MallOrder.getOrderStatus() != 1) {
                    errorOrderNos += MallOrder.getOrderNo() + " ";
                }
            }
            if (StringUtils.isEmpty(errorOrderNos)) {
                if (MallOrderMapper.checkDone(Arrays.asList(ids)) > 0) {
                    return ServiceResultEnum.SUCCESS.getResult();
                } else {
                    return ServiceResultEnum.DB_ERROR.getResult();
                }
            } else {
                if (errorOrderNos.length() > 0 && errorOrderNos.length() < 100) {
                    return errorOrderNos + "Error: Order have not paid or have already shipped";
                } else {
                    return "Error: Order have not paid or have already shipped";
                }
            }
        }
        return ServiceResultEnum.DATA_NOT_EXIST.getResult();
    }

    @Override
    @Transactional
    public String checkOut(Long[] ids) {
        List<MallOrder> orders = MallOrderMapper.selectByPrimaryKeys(Arrays.asList(ids));
        String errorOrderNos = "";
        if (!CollectionUtils.isEmpty(orders)) {
            for (MallOrder MallOrder : orders) {
                if (MallOrder.getIsDeleted() == 1) {
                    errorOrderNos += MallOrder.getOrderNo() + " ";
                    continue;
                }
                if (MallOrder.getOrderStatus() != 1 && MallOrder.getOrderStatus() != 2) {
                    errorOrderNos += MallOrder.getOrderNo() + " ";
                }
            }
            if (StringUtils.isEmpty(errorOrderNos)) {
                if (MallOrderMapper.checkOut(Arrays.asList(ids)) > 0) {
                    return ServiceResultEnum.SUCCESS.getResult();
                } else {
                    return ServiceResultEnum.DB_ERROR.getResult();
                }
            } else {
                if (errorOrderNos.length() > 0 && errorOrderNos.length() < 100) {
                    return errorOrderNos + "Error: Order have not paid or have already shipped";
                } else {
                    return "Error: Order have not paid or have already shipped";
                }
            }
        }
        return ServiceResultEnum.DATA_NOT_EXIST.getResult();
    }

    @Override
    @Transactional
    public String closeOrder(Long[] ids) {
        List<MallOrder> orders = MallOrderMapper.selectByPrimaryKeys(Arrays.asList(ids));
        String errorOrderNos = "";
        if (!CollectionUtils.isEmpty(orders)) {
            for (MallOrder MallOrder : orders) {
                // isDeleted=1, order have been canceled
                if (MallOrder.getIsDeleted() == 1) {
                    errorOrderNos += MallOrder.getOrderNo() + " ";
                    continue;
                }
                if (MallOrder.getOrderStatus() == 4 || MallOrder.getOrderStatus() < 0) {
                    errorOrderNos += MallOrder.getOrderNo() + " ";
                }
            }
            if (StringUtils.isEmpty(errorOrderNos)) {
                if (MallOrderMapper.closeOrder(Arrays.asList(ids), MallOrderStatusEnum.ORDER_CLOSED_BY_JUDGE.getOrderStatus()) > 0) {
                    return ServiceResultEnum.SUCCESS.getResult();
                } else {
                    return ServiceResultEnum.DB_ERROR.getResult();
                }
            } else {
                if (errorOrderNos.length() > 0 && errorOrderNos.length() < 100) {
                    return errorOrderNos + "Fail to cancel the order";
                } else {
                    return "Fail to cancel the order";
                }
            }
        }
        //return error
        return ServiceResultEnum.DATA_NOT_EXIST.getResult();
    }

    @Override
    @Transactional
    public String saveOrder(MallUserVO user, List<MallShoppingCartItemVO> myShoppingCartItems) {
        List<Long> itemIdList = myShoppingCartItems.stream().map(MallShoppingCartItemVO::getCartItemId).collect(Collectors.toList());
        List<Long> goodsIds = myShoppingCartItems.stream().map(MallShoppingCartItemVO::getGoodsId).collect(Collectors.toList());
        List<MallGoods> mirrarMallGoods = MallGoodsMapper.selectByPrimaryKeys(goodsIds);
        List<MallGoods> goodsListNotSelling = mirrarMallGoods.stream()
                .filter(MallGoodsTemp -> MallGoodsTemp.getGoodsSellStatus() != Constants.SELL_STATUS_UP)
                .collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(goodsListNotSelling)) {
            MallException.fail(goodsListNotSelling.get(0).getGoodsName() + "product is unavailable");
        }
        Map<Long, MallGoods> MallGoodsMap = mirrarMallGoods.stream().collect(Collectors.toMap(MallGoods::getGoodsId, Function.identity(), (entity1, entity2) -> entity1));
        for (MallShoppingCartItemVO shoppingCartItemVO : myShoppingCartItems) {
            if (!MallGoodsMap.containsKey(shoppingCartItemVO.getGoodsId())) {
                MallException.fail(ServiceResultEnum.SHOPPING_ITEM_ERROR.getResult());
            }
            if (shoppingCartItemVO.getGoodsCount() > MallGoodsMap.get(shoppingCartItemVO.getGoodsId()).getStockNum()) {
                MallException.fail(ServiceResultEnum.SHOPPING_ITEM_COUNT_ERROR.getResult());
            }
        }
        if (!CollectionUtils.isEmpty(itemIdList) && !CollectionUtils.isEmpty(goodsIds) && !CollectionUtils.isEmpty(mirrarMallGoods)) {
            if (MallShoppingCartItemMapper.deleteBatch(itemIdList) > 0) {
                List<StockNumDTO> stockNumDTOS = BeanUtil.copyList(myShoppingCartItems, StockNumDTO.class);
                int updateStockNumResult = MallGoodsMapper.updateStockNum(stockNumDTOS);
                if (updateStockNumResult < 1) {
                   MallException.fail(ServiceResultEnum.SHOPPING_ITEM_COUNT_ERROR.getResult());
                }
                //Generate orderNos
                String orderNo = NumberUtil.genOrderNo();
                int priceTotal = 0;
                MallOrder MallOrder = new MallOrder();
                MallOrder.setOrderNo(orderNo);
                MallOrder.setUserId(user.getUserId());
                MallOrder.setUserAddress(user.getAddress());
                //total price
                for (MallShoppingCartItemVO MallShoppingCartItemVO : myShoppingCartItems) {
                    priceTotal += MallShoppingCartItemVO.getGoodsCount() * MallShoppingCartItemVO.getSellingPrice();
                }
                if (priceTotal < 1) {
                    MallException.fail(ServiceResultEnum.ORDER_PRICE_ERROR.getResult());
                }
                MallOrder.setTotalPrice(priceTotal);
                //todo generate new oder info
                String extraInfo = "";
                MallOrder.setExtraInfo(extraInfo);
                if (MallOrderMapper.insertSelective(MallOrder) > 0) {
                    List<MallOrderItem> MallOrderItems = new ArrayList<>();
                    for (MallShoppingCartItemVO MallShoppingCartItemVO : myShoppingCartItems) {
                        MallOrderItem MallOrderItem = new MallOrderItem();
                        BeanUtil.copyProperties(MallShoppingCartItemVO, MallOrderItem);
                        MallOrderItem.setOrderId(MallOrder.getOrderId());
                        MallOrderItems.add(MallOrderItem);
                    }
                    if (MallOrderItemMapper.insertBatch(MallOrderItems) > 0) {
                        return orderNo;
                    }
                    MallException.fail(ServiceResultEnum.ORDER_PRICE_ERROR.getResult());
                }
                MallException.fail(ServiceResultEnum.DB_ERROR.getResult());
            }
            MallException.fail(ServiceResultEnum.DB_ERROR.getResult());
        }
        MallException.fail(ServiceResultEnum.SHOPPING_ITEM_ERROR.getResult());
        return ServiceResultEnum.SHOPPING_ITEM_ERROR.getResult();
    }

    @Override
    public MallOrderDetailVO getOrderDetailByOrderNo(String orderNo, Long userId) {
        MallOrder MallOrder = MallOrderMapper.selectByOrderNo(orderNo);
        if (MallOrder != null) {
            //todo check userID
            List<MallOrderItem> orderItems = MallOrderItemMapper.selectByOrderId(MallOrder.getOrderId());
            if (!CollectionUtils.isEmpty(orderItems)) {
                List<MallOrderItemVO> MallOrderItemVOS = BeanUtil.copyList(orderItems, MallOrderItemVO.class);
                MallOrderDetailVO MallOrderDetailVO = new MallOrderDetailVO();
                BeanUtil.copyProperties(MallOrder, MallOrderDetailVO);
                MallOrderDetailVO.setOrderStatusString(MallOrderStatusEnum.getMallOrderStatusEnumByStatus(MallOrderDetailVO.getOrderStatus()).getName());
                MallOrderDetailVO.setPayTypeString(PayTypeEnum.getPayTypeEnumByType(MallOrderDetailVO.getPayType()).getName());
                MallOrderDetailVO.setMallOrderItemVOS(MallOrderItemVOS);
                return MallOrderDetailVO;
            }
        }
        return null;
    }

    @Override
    public MallOrder getMallOrderByOrderNo(String orderNo) {
        return MallOrderMapper.selectByOrderNo(orderNo);
    }

    @Override
    public PageResult getMyOrders(PageQueryUtil pageUtil) {
        int total = MallOrderMapper.getTotalMallOrders(pageUtil);
        List<MallOrder> MallOrders = MallOrderMapper.findMallOrderList(pageUtil);
        List<MallOrderListVO> orderListVOS = new ArrayList<>();
        if (total > 0) {
            orderListVOS = BeanUtil.copyList(MallOrders, MallOrderListVO.class);
            for (MallOrderListVO MallOrderListVO : orderListVOS) {
                MallOrderListVO.setOrderStatusString(MallOrderStatusEnum.getMallOrderStatusEnumByStatus(MallOrderListVO.getOrderStatus()).getName());
            }
            List<Long> orderIds = MallOrders.stream().map(MallOrder::getOrderId).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(orderIds)) {
                List<MallOrderItem> orderItems = MallOrderItemMapper.selectByOrderIds(orderIds);
                Map<Long, List<MallOrderItem>> itemByOrderIdMap = orderItems.stream().collect(groupingBy(MallOrderItem::getOrderId));
                for (MallOrderListVO MallOrderListVO : orderListVOS) {
                    if (itemByOrderIdMap.containsKey(MallOrderListVO.getOrderId())) {
                        List<MallOrderItem> orderItemListTemp = itemByOrderIdMap.get(MallOrderListVO.getOrderId());
                        List<MallOrderItemVO> MallOrderItemVOS = BeanUtil.copyList(orderItemListTemp, MallOrderItemVO.class);
                        MallOrderListVO.setMallOrderItemVOS(MallOrderItemVOS);
                    }
                }
            }
        }
        PageResult pageResult = new PageResult(orderListVOS, total, pageUtil.getLimit(), pageUtil.getPage());
        return pageResult;
    }

    @Override
    public String cancelOrder(String orderNo, Long userId) {
        MallOrder MallOrder = MallOrderMapper.selectByOrderNo(orderNo);
        if (MallOrder != null) {
            //todo check userID
            //todo check order status
            if (MallOrderMapper.closeOrder(Collections.singletonList(MallOrder.getOrderId()), MallOrderStatusEnum.ORDER_CLOSED_BY_MALLUSER.getOrderStatus()) > 0) {
                return ServiceResultEnum.SUCCESS.getResult();
            } else {
                return ServiceResultEnum.DB_ERROR.getResult();
            }
        }
        return ServiceResultEnum.ORDER_NOT_EXIST_ERROR.getResult();
    }

    @Override
    public String finishOrder(String orderNo, Long userId) {
        MallOrder MallOrder = MallOrderMapper.selectByOrderNo(orderNo);
        if (MallOrder != null) {
            //todo check userID
            //todo check order status
            MallOrder.setOrderStatus((byte) MallOrderStatusEnum.ORDER_SUCCESS.getOrderStatus());
            MallOrder.setUpdateTime(new Date());
            if (MallOrderMapper.updateByPrimaryKeySelective(MallOrder) > 0) {
                return ServiceResultEnum.SUCCESS.getResult();
            } else {
                return ServiceResultEnum.DB_ERROR.getResult();
            }
        }
        return ServiceResultEnum.ORDER_NOT_EXIST_ERROR.getResult();
    }

    @Override
    public String paySuccess(String orderNo, int payType) {
        MallOrder MallOrder = MallOrderMapper.selectByOrderNo(orderNo);
        if (MallOrder != null) {
            //todo check order sattus
            MallOrder.setOrderStatus((byte) MallOrderStatusEnum.OREDER_PAID.getOrderStatus());
            MallOrder.setPayType((byte) payType);
            MallOrder.setPayStatus((byte) PayStatusEnum.PAY_SUCCESS.getPayStatus());
            MallOrder.setPayTime(new Date());
            MallOrder.setUpdateTime(new Date());
            if (MallOrderMapper.updateByPrimaryKeySelective(MallOrder) > 0) {
                return ServiceResultEnum.SUCCESS.getResult();
            } else {
                return ServiceResultEnum.DB_ERROR.getResult();
            }
        }
        return ServiceResultEnum.ORDER_NOT_EXIST_ERROR.getResult();
    }

    @Override
    public List<MallOrderItemVO> getOrderItems(Long id) {
        MallOrder MallOrder = MallOrderMapper.selectByPrimaryKey(id);
        if (MallOrder != null) {
            List<MallOrderItem> orderItems = MallOrderItemMapper.selectByOrderId(MallOrder.getOrderId());
            if (!CollectionUtils.isEmpty(orderItems)) {
                List<MallOrderItemVO> MallOrderItemVOS = BeanUtil.copyList(orderItems,MallOrderItemVO.class);
                return MallOrderItemVOS;
            }
        }
        return null;
    }
}

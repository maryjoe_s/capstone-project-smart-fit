package ltd.mirrar.mall.service.impl;

import ltd.mirrar.mall.common.Constants;
import ltd.mirrar.mall.common.ServiceResultEnum;
import ltd.mirrar.mall.controller.vo.MallShoppingCartItemVO;
import ltd.mirrar.mall.dao.MallGoodsMapper;
import ltd.mirrar.mall.dao.MallShoppingCartItemMapper;
import ltd.mirrar.mall.entity.MallGoods;
import ltd.mirrar.mall.entity.MallShoppingCartItem;
import ltd.mirrar.mall.service.MallShoppingCartService;
import ltd.mirrar.mall.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class MallShoppingCartServiceImpl implements MallShoppingCartService {

    @Autowired
    private MallShoppingCartItemMapper MallShoppingCartItemMapper;

    @Autowired
    private MallGoodsMapper MallGoodsMapper;

    //todo update items number in shopping cart

    @Override
    public String saveMallCartItem(MallShoppingCartItem MallShoppingCartItem) {
        MallShoppingCartItem temp = MallShoppingCartItemMapper.selectByUserIdAndGoodsId(MallShoppingCartItem.getUserId(), MallShoppingCartItem.getGoodsId());
        if (temp != null) {
            //todo count = tempCount + 1
            temp.setGoodsCount(MallShoppingCartItem.getGoodsCount());
            return updateMallCartItem(temp);
        }
        MallGoods MallGoods = MallGoodsMapper.selectByPrimaryKey(MallShoppingCartItem.getGoodsId());
        if (MallGoods == null) {
            return ServiceResultEnum.GOODS_NOT_EXIST.getResult();
        }
        int totalItem = MallShoppingCartItemMapper.selectCountByUserId(MallShoppingCartItem.getUserId()) + 1;
        if (MallShoppingCartItem.getGoodsCount() > Constants.SHOPPING_CART_ITEM_LIMIT_NUMBER) {
            return ServiceResultEnum.SHOPPING_CART_ITEM_LIMIT_NUMBER_ERROR.getResult();
        }
        if (totalItem > Constants.SHOPPING_CART_ITEM_TOTAL_NUMBER) {
            return ServiceResultEnum.SHOPPING_CART_ITEM_TOTAL_NUMBER_ERROR.getResult();
        }
        if (MallShoppingCartItemMapper.insertSelective(MallShoppingCartItem) > 0) {
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    public String updateMallCartItem(MallShoppingCartItem MallShoppingCartItem) {
        MallShoppingCartItem MallShoppingCartItemUpdate = MallShoppingCartItemMapper.selectByPrimaryKey(MallShoppingCartItem.getCartItemId());
        if (MallShoppingCartItemUpdate == null) {
            return ServiceResultEnum.DATA_NOT_EXIST.getResult();
        }
        if (MallShoppingCartItem.getGoodsCount() > Constants.SHOPPING_CART_ITEM_LIMIT_NUMBER) {
            return ServiceResultEnum.SHOPPING_CART_ITEM_LIMIT_NUMBER_ERROR.getResult();
        }
        //todo check item number
        //todo check userId
        MallShoppingCartItemUpdate.setGoodsCount(MallShoppingCartItem.getGoodsCount());
        MallShoppingCartItemUpdate.setUpdateTime(new Date());
        if (MallShoppingCartItemMapper.updateByPrimaryKeySelective(MallShoppingCartItemUpdate) > 0) {
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    public MallShoppingCartItem getMallCartItemById(Long MallShoppingCartItemId) {
        return MallShoppingCartItemMapper.selectByPrimaryKey(MallShoppingCartItemId);
    }

    @Override
    public Boolean deleteById(Long MallShoppingCartItemId) {
        //todo check userId
        return MallShoppingCartItemMapper.deleteByPrimaryKey(MallShoppingCartItemId) > 0;
    }

    @Override
    public List<MallShoppingCartItemVO> getMyShoppingCartItems(Long MallUserId) {
        List<MallShoppingCartItemVO> MallShoppingCartItemVOS = new ArrayList<>();
        List<MallShoppingCartItem> MallShoppingCartItems = MallShoppingCartItemMapper.selectByUserId(MallUserId, Constants.SHOPPING_CART_ITEM_TOTAL_NUMBER);
        if (!CollectionUtils.isEmpty(MallShoppingCartItems)) {
            List<Long> MallGoodsIds = MallShoppingCartItems.stream().map(MallShoppingCartItem::getGoodsId).collect(Collectors.toList());
            List<MallGoods> mirrarMallGoods = MallGoodsMapper.selectByPrimaryKeys(MallGoodsIds);
            Map<Long, MallGoods> MallGoodsMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(mirrarMallGoods)) {
                MallGoodsMap = mirrarMallGoods.stream().collect(Collectors.toMap(MallGoods::getGoodsId, Function.identity(), (entity1, entity2) -> entity1));
            }
            for (MallShoppingCartItem MallShoppingCartItem : MallShoppingCartItems) {
                MallShoppingCartItemVO MallShoppingCartItemVO = new MallShoppingCartItemVO();
                BeanUtil.copyProperties(MallShoppingCartItem, MallShoppingCartItemVO);
                if (MallGoodsMap.containsKey(MallShoppingCartItem.getGoodsId())) {
                    MallGoods MallGoodsTemp = MallGoodsMap.get(MallShoppingCartItem.getGoodsId());
                    MallShoppingCartItemVO.setGoodsCoverImg(MallGoodsTemp.getGoodsCoverImg());
                    String goodsName = MallGoodsTemp.getGoodsName();
                    if (goodsName.length() > 28) {
                        goodsName = goodsName.substring(0, 28) + "...";
                    }
                    MallShoppingCartItemVO.setGoodsName(goodsName);
                    MallShoppingCartItemVO.setSellingPrice(MallGoodsTemp.getSellingPrice());
                    MallShoppingCartItemVOS.add(MallShoppingCartItemVO);
                }
            }
        }
        return MallShoppingCartItemVOS;
    }
}

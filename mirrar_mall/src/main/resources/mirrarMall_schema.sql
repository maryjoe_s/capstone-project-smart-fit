SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `tb_admin_user`;
CREATE TABLE `tb_admin_user`  (
  `admin_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `locked` tinyint(4) NULL DEFAULT 0 COMMENT '0-unlock 1-locked',
  PRIMARY KEY (`admin_user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


INSERT INTO `tb_admin_user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'jingyu', 0);
INSERT INTO `tb_admin_user` VALUES (2, 'admin1', 'e10adc3949ba59abbe56e057f20f883e', 'admin1', 0);
INSERT INTO `tb_admin_user` VALUES (3, 'admin2', 'e10adc3949ba59abbe56e057f20f883e', 'admin2', 0);

DROP TABLE IF EXISTS `tb_carousel`;
CREATE TABLE `tb_carousel`  (
  `carousel_id` int(11) NOT NULL AUTO_INCREMENT,
  `carousel_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `redirect_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '\'##\'' ,
  `carousel_rank` int(11) NOT NULL DEFAULT 0 ,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-notDelete 1-deleted',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` int(11) NOT NULL DEFAULT 0 ,
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` int(11) NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`carousel_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


INSERT INTO `tb_carousel` VALUES (1, '/goods-img/banner/banner01.jpg', '', 2, 0, '2021-03-30 00:00:00', 0, '2021-03-30 00:00:00', 0);
INSERT INTO `tb_carousel` VALUES (2, '/goods-img/banner/banner02.jpg', '', 1, 0, '2021-03-30 00:00:00', 0, '2021-03-30 00:00:00', 0);


DROP TABLE IF EXISTS `tb_goods_category`;
CREATE TABLE `tb_goods_category`  (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT ,
  `category_level` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1-firstLevel 2-secondeLevel 3-thirdLevel)',
  `parent_id` bigint(20) NOT NULL DEFAULT 0 ,
  `category_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `category_rank` int(11) NOT NULL DEFAULT 0 ,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_user` int(11) NOT NULL DEFAULT 0,
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 107 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- first level
 INSERT INTO `tb_goods_category` VALUES (111, 1, 0, 'ACCESSORISES', 100, 0, '2021-03-30 18:45:40', 0, '2021-03-30 18:45:40', 0);
 INSERT INTO `tb_goods_category` VALUES (112, 1, 0, 'BAGS', 99, 0, '2021-03-30 18:45:40', 0, '2021-03-30 18:45:40', 0);
 INSERT INTO `tb_goods_category` VALUES (113, 1, 0, 'CLOUTHING', 98, 0, '2021-03-30 18:45:40', 0, '2021-03-30 18:45:40', 0);
 INSERT INTO `tb_goods_category` VALUES (114, 1, 0, 'SHOSES', 97, 0, '2021-03-30 18:45:40', 0, '2021-03-30 18:45:40', 0);


-- second level
INSERT INTO `tb_goods_category` VALUES (211, 2, 111, 'EYEWEAR', 0, 0, '2021-03-30 18:46:32', 0, '2021-03-30 18:46:32', 0);
INSERT INTO `tb_goods_category` VALUES (212, 2, 111, 'HAT', 0, 0, '2021-03-30 18:46:32', 0, '2021-03-30 18:46:32', 0);
INSERT INTO `tb_goods_category` VALUES (221, 2, 112, 'WOMEN', 0, 0, '2021-03-30 18:46:52', 0, '2021-03-30 18:46:52', 0);
INSERT INTO `tb_goods_category` VALUES (222, 2, 112, 'MEN', 0, 0, '2021-03-30 18:46:52', 0, '2021-03-30 18:46:52', 0);
INSERT INTO `tb_goods_category` VALUES (231, 2, 113, 'WOMEN', 10, 0, '2021-03-30 00:15:19', 0, '2021-03-30 00:15:19', 0);
INSERT INTO `tb_goods_category` VALUES (232, 2, 113, 'MEN', 10, 0, '2021-03-30 00:15:19', 0, '2021-03-30 00:15:19', 0);
INSERT INTO `tb_goods_category` VALUES (241, 2, 114, 'WOMEN', 10, 0, '2021-03-30 00:15:19', 0, '2021-03-30 00:15:19', 0);
INSERT INTO `tb_goods_category` VALUES (242, 2, 114, 'MEN', 10, 0, '2021-03-30 00:15:19', 0, '2021-03-30 00:15:19', 0);


-- third level
INSERT INTO `tb_goods_category` VALUES (311, 3, 211, 'Sun Glasses', 0, 0, '2021-03-30 18:47:38', 0, '2021-03-30 18:47:38', 0);
INSERT INTO `tb_goods_category` VALUES (312, 3, 211, 'Glasses', 0, 0, '2021-03-30 18:47:38', 0, '2021-03-30 18:47:38', 0);
INSERT INTO `tb_goods_category` VALUES (313, 3, 212, 'Beach Hats', 0, 0, '2021-03-30 18:47:38', 0, '2021-03-30 18:47:38', 0);
INSERT INTO `tb_goods_category` VALUES (314, 3, 212, 'Caps', 0, 0, '2021-03-30 18:47:38', 0, '2021-03-30 18:47:38', 0);
INSERT INTO `tb_goods_category` VALUES (321, 3, 221, 'Backpacks', 0, 0, '2021-03-30 18:47:49', 0, '2021-03-30 18:47:49', 0);
INSERT INTO `tb_goods_category` VALUES (322, 3, 221, 'Shoulder Bags', 0, 0, '2021-03-30 18:47:49', 0, '2021-03-30 18:47:49', 0);
INSERT INTO `tb_goods_category` VALUES (323, 3, 222, 'Backpacks', 0, 0, '2021-03-30 18:47:49', 0, '2021-03-30 18:47:49', 0);
INSERT INTO `tb_goods_category` VALUES (331, 3, 231, 'Tops', 0, 0, '2021-03-30 18:47:58', 0, '2021-03-30 18:47:58', 0);
INSERT INTO `tb_goods_category` VALUES (332, 3, 231, 'Dresss', 0, 0, '2021-03-30 18:47:58', 0, '2021-03-30 18:47:58', 0);
INSERT INTO `tb_goods_category` VALUES (333, 3, 231, 'Pants', 0, 0, '2021-03-30 18:47:58', 0, '2021-03-30 18:47:58', 0);
INSERT INTO `tb_goods_category` VALUES (334, 3, 231, 'Jackets', 0, 0, '2021-03-30 18:47:58', 0, '2021-03-30 18:47:58', 0);
INSERT INTO `tb_goods_category` VALUES (335, 3, 231, 'Jeans', 0, 0, '2021-03-30 18:47:58', 0, '2021-03-30 18:47:58', 0);
INSERT INTO `tb_goods_category` VALUES (336, 3, 231, 'Shorts', 0, 0, '2021-03-30 18:47:58', 0, '2021-03-30 18:47:58', 0);
INSERT INTO `tb_goods_category` VALUES (337, 3, 232, 'Tops', 0, 0, '2021-03-30 18:47:58', 0, '2021-03-30 18:47:58', 0);
INSERT INTO `tb_goods_category` VALUES (338, 3, 232, 'Pants', 0, 0, '2021-03-30 18:47:58', 0, '2021-03-30 18:47:58', 0);
INSERT INTO `tb_goods_category` VALUES (339, 3, 232, 'Jeans', 0, 0, '2021-03-30 18:47:58', 0, '2021-03-30 18:47:58', 0);
INSERT INTO `tb_goods_category` VALUES (341, 3, 241, 'Boots', 0, 0, '2021-03-30 18:49:19', 0, '2021-03-30 18:49:19', 0);
INSERT INTO `tb_goods_category` VALUES (342, 3, 241, 'Sandals', 0, 0, '2021-03-30 18:49:19', 0, '2021-03-30 18:49:19', 0);
INSERT INTO `tb_goods_category` VALUES (343, 3, 241, 'Sneakers', 0, 0, '2021-03-30 18:49:19', 0, '2021-03-30 18:49:19', 0);
INSERT INTO `tb_goods_category` VALUES (344, 3, 242, 'Boots', 0, 0, '2021-03-30 18:49:19', 0, '2021-03-30 18:49:19', 0);
INSERT INTO `tb_goods_category` VALUES (345, 3, 242, 'Sneakers', 0, 0, '2021-03-30 18:49:19', 0, '2021-03-30 18:49:19', 0);



DROP TABLE IF EXISTS `tb_goods_info`;
CREATE TABLE `tb_goods_info`  (
  `goods_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `goods_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `goods_intro` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `goods_category_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '关联分类id',
  `goods_cover_img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '/admin/dist/img/no-img.png',
  `goods_carousel` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '/admin/dist/img/no-img.png' ,
  `goods_detail_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `original_price` int(11) NOT NULL DEFAULT 1,
  `selling_price` int(11) NOT NULL DEFAULT 1,
  `stock_num` int(11) NOT NULL DEFAULT 0 ,
  `tag` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `goods_sell_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-available 1-unavailable',
  `create_user` int(11) NOT NULL DEFAULT 0 ,
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `update_user` int(11) NOT NULL DEFAULT 0 ,
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`goods_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10896 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `tb_goods_info` (`goods_id`, `goods_name`, `goods_intro`, `goods_category_id`, `goods_cover_img`, `goods_carousel`, `goods_detail_content`, `original_price`, `selling_price`, `stock_num`, `tag`, `goods_sell_status`, `create_user`, `create_time`, `update_user`, `update_time`)
VALUES
	(100001,'SWEAT LONG SLEEVE PULLOVER T-Shirt','A comfy hoodie in thick sweatshirt fabric with a casual feel.',331,'/goods-img/good/goods_02_437374.png','/goods-img/good/goods_02_437374.png','...',39.90,39.90,1000,'',0,0,'2021-03-30 13:18:47',0,'2021-03-30 10:41:59'),
	(100002,'JERSEY RELAXED T-Shirt','Light slip-on jacket that soft and comfortable. Can pair with any bottoms.',334,'/goods-img/good/goods_52_433642.png','/goods-img/good/goods_52_433642.png','...',39.90,39.90,1000,'',0,0,'2021-03-30 13:18:47',0,'2021-03-30 10:41:59'),
	(100003,'LINEN BLEND SHORT SLEEVE KAFTAN DRESS','The smooth feel of rayon with the texture of linen. In a relaxed cut with a lovely drape.',332,'/goods-img/good/goods_34_433661.png','/goods-img/good/goods_34_433661.png','...',49.90,49.90,1000,'',0,0,'2021-03-30 13:18:47',0,'2021-03-30 10:41:59'),
	(100004,'DRAPE TWILL TAPERED PANTS','Silky texture you can dress up or down. Elastic waist.',333,'/goods-img/good/goods_30_436566.png','/goods-img/good/goods_30_436566.png','...',39.90,39.90,1000,'',0,0,'2021-03-30 13:18:47',0,'2021-03-30 10:41:59'),
	(100005,'RELAXED TAPERED ANKLE JEANS','Popular relaxed design. Tapered ankle-length for a clean look.',335,'/goods-img/good/goods_64_438681.jpeg','/goods-img/good/goods_64_438681.jpeg','...',49.90,49.90,1000,'',0,0,'2021-03-30 13:18:47',0,'2021-03-30 10:41:59'),
	(100006,'BELTED SMART SHORTS','High-rise with a belt design for an even trendier look. Supple material that gives them a refined feel.',336,'/goods-img/good/goods_31_425547.jpeg','/goods-img/good/goods_31_425547.jpeg','...',22.90,22.90,1000,'',0,0,'2021-03-30 13:18:47',0,'2021-03-30 10:41:59'),
	(100007,'U AIRISM COTTON CREW NECK OVERSIZED T-SHIRT','Feel smooth, AIRism with cotton looks.A fashionable oversized design.',337,'/goods-img/good/goods_37_425974.jpeg','/goods-img/good/goods_37_425974.jpeg','...',24.90,24.90,1000,'',0,0,'2021-03-30 13:18:47',0,'2021-03-30 10:41:59'),
	(100008,'UNIQLO U REGULAR FIT WORK PANTS','Made with a durable material with a sharp look. Carefully designed to last long.',338,'/goods-img/good/goods_34_437409.png','/goods-img/good/goods_34_437409.jpeg','...',64.90,64.90,1000,'',0,0,'2021-03-30 13:18:47',0,'2021-03-30 10:41:59'),
	(100009,'EZY JEANS (TALL)','As comfortable as sweats but with a classic look. A new type of jeans that you will find yourself wearing again and again.',339,'/goods-img/good/goods_60_431839.jpeg','/goods-img/good/goods_60_431839.jpeg','...',69.90,69.90,1000,'',0,0,'2021-03-30 13:18:47',0,'2021-03-30 10:41:59');



DROP TABLE IF EXISTS `tb_index_config`;
CREATE TABLE `tb_index_config`  (
  `config_id` bigint(20) NOT NULL AUTO_INCREMENT  ,
  `config_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `config_type` tinyint(4) NOT NULL DEFAULT 0 ,
  `goods_id` bigint(20) NOT NULL DEFAULT 0 ,
  `redirect_url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '##' ,
  `config_rank` int(11) NOT NULL DEFAULT 0 ,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 ,
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `create_user` int(11) NOT NULL DEFAULT 0 ,
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_user` int(11) NULL DEFAULT 0 ,
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `tb_index_config` VALUES (1, 'SWEAT LONG SLEEVE PULLOVER HOODIE', 3, 100001, '##', 100, 0, '2021-03-30 17:05:27', 0, '2021-03-30 17:05:27', 0);
INSERT INTO `tb_index_config` VALUES (2, 'JERSEY RELAXED JACKET', 3, 100002, '##', 100, 0, '2021-03-30 17:05:27', 0, '2021-03-30 17:05:27', 0);
INSERT INTO `tb_index_config` VALUES (3, 'LINEN BLEND SHORT SLEEVE KAFTAN DRESS', 3, 100003, '##', 100, 0, '2021-03-30 17:05:27', 0, '2021-03-30 17:05:27', 0);
INSERT INTO `tb_index_config` VALUES (4, 'DRAPE TWILL TAPERED PANTS', 3, 100004, '##', 100, 0, '2021-03-30 17:05:27', 0, '2021-03-30 17:05:27', 0);


DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order`  (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT ,
  `order_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `user_id` bigint(20) NOT NULL DEFAULT 0 ,
  `total_price` int(11) NOT NULL DEFAULT 1 ,
  `pay_status` tinyint(4) NOT NULL DEFAULT 0 ,
  `pay_type` tinyint(4) NOT NULL DEFAULT 0 ,
  `pay_time` datetime(0) NULL DEFAULT NULL ,
  `order_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0.waitForPay 1.Paid 2.ProductReady 3:Shipped 4.Completed -1.Canceled -2.Overtime -3.CanceledbyShop',
  `extra_info` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `user_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `user_address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-notDelete 1-deleted',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

 INSERT INTO `tb_order` VALUES (1, '15688187285093508', 1, 79.8, 1, 2, '2021-03-30 23:00:18', 1, '', '', '', 'Main Street, Ottawa, Ontaria', 0, '2021-03-30 23:01:00', '2021-03-30 23:01:00');


DROP TABLE IF EXISTS `tb_order_item`;
CREATE TABLE `tb_order_item`  (
  `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT ,
  `order_id` bigint(20) NOT NULL DEFAULT 0,
  `goods_id` bigint(20) NOT NULL DEFAULT 0 ,
  `goods_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `goods_cover_img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `selling_price` int(11) NOT NULL DEFAULT 1 ,
  `goods_count` int(11) NOT NULL DEFAULT 1 ,
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`order_item_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `tb_order_item` VALUES (1, 1, 1000001, 'SWEAT LONG SLEEVE PULLOVER HOODIE', '/goods-img/good/goods_02_437374.jpeg', 39.9, 2, '2021-03-30 22:53:07');


DROP TABLE IF EXISTS `tb_shopping_cart_item`;
CREATE TABLE `tb_shopping_cart_item`  (
  `cart_item_id` bigint(20) NOT NULL AUTO_INCREMENT ,
  `user_id` bigint(20) NOT NULL ,
  `goods_id` bigint(20) NOT NULL DEFAULT 0 ,
  `goods_count` int(11) NOT NULL DEFAULT 1 ,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 ,
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  PRIMARY KEY (`cart_item_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;



DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT ,
  `nick_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `login_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `password_md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `introduce_sign` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-notDelete 1-deleted',
  `locked_flag` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0-unlock 1-locked',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


INSERT INTO `tb_user` VALUES (1, 'Jingyu', 'Jingyu', 'e10adc3949ba59abbe56e057f20f883e', 'hello', 'Main Street, Ottawa, Ontaria', 0, 0, '2021-03-30 08:44:57');
INSERT INTO `tb_user` VALUES (2, 'test1', 'test', 'e10adc3949ba59abbe56e057f20f883e', 'test1', '999 Second Avenue, Ottawa, Ontaria', 0, 0, '2021-03-30 10:51:39');